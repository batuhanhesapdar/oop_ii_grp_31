﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_Application
{
    interface IOperation
    {
        float doOperation(float num1, float num2);
    }

    class AdditionStrategy: IOperation
    {
        public float doOperation(float num1, float num2)
        {
            return num1 + num2;
        }
    }
    class SubstractionStrategy : IOperation
    {
        public float doOperation(float num1, float num2)
        {
            return num1 - num2;
        }
    }
    class MultiplicationStrategy : IOperation
    {
        public float doOperation(float num1, float num2)
        {
            return num1 * num2;
        }
    }
    class DivisionStrategy : IOperation
    {
        public float doOperation(float num1, float num2)
        {
            return num1 / num2;
        }
    }
    class OperationContext
    {
        private IOperation operation;
        public OperationContext(IOperation _operation)
        {
            this.operation = _operation;
        }
        public float executeStrategy(float num1, float num2)
        {
            return this.operation.doOperation(num1, num2);
        }
    }
}
