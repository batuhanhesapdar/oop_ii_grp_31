﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Drawing;

namespace Lab_Application
{
    /// <summary>
    /// Basic operations
    /// </summary>
    public class Calculator
    {
        private static TextBox Num1;
        private static TextBox Num2;
        private static Label Result;
        private static float res;
        private static OperationContext context;

        public Calculator(TextBox num1, TextBox num2, Label result)
        {
            Num1 = num1;
            Num2 = num2;
            Result = result;
        }
        public static void OperationHandler(string operation)
        {
            // Don't allow to empty operands
            if (Num1.Text.Length == 0 || Num2.Text.Length == 0)
            {
                Result.BackColor = Color.White;
                Result.Text = "Empty operands!";
                return;
            }

            float operand1 = float.Parse(Num1.Text);
            float operand2 = float.Parse(Num2.Text);

            if (operation == "Add")
            {
                context = new OperationContext(new AdditionStrategy());
                res = context.executeStrategy(operand1, operand2);
            }
            if (operation == "Substract")
            {
                context = new OperationContext(new SubstractionStrategy());
                res = context.executeStrategy(operand1, operand2);
            }
            if (operation == "Multiply")
            {
                context = new OperationContext(new MultiplicationStrategy());
                res = context.executeStrategy(operand1, operand2);
            }
            if (operation == "Divide")
            {
                // Divided by 0 error
                if (operand2 == 0)
                {
                    Result.BackColor = Color.White;
                    Result.Text = "Error -> Divided by 0!";
                    return;
                }
                context = new OperationContext(new DivisionStrategy());
                res = context.executeStrategy(operand1, operand2);
            }
            ShowResult(res);
        }
        /// <summary>
        /// A private method to show the result of an operation in the class
        /// and in a label with different properties
        /// </summary>
        /// <param name="result">Result of an operation</param>
        private static void ShowResult(float result)
        {
            if (result <= 0)
            {
                Result.BackColor = Color.Red;
                Result.Text = result.ToString();
            }
            else
            {
                Result.BackColor = Color.Green;
                Result.Text = result.ToString();
            }
        }
    }
}
