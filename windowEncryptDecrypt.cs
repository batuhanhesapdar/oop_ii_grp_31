﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;


namespace Lab_Application
{
    public partial class windowEncryptDecrypt : Form
    {
        public windowEncryptDecrypt()
        {
            InitializeComponent();
            // Disabling groupboxes and their elements at the begining
            grpBoxAlgo.Enabled = false;
            grpBoxOps.Enabled = false;
            lblROT.Visible = false;
            txtROT.Visible = false;
            lblKey.Visible = false;
            txtKey.Visible = false;
            // Default Alphabet
            txtAlphabet.Text = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        }

        private void radioBtnAlgorithm_CheckedChanged(object sender, EventArgs e)
        {
            // Display the ROT or key value textbox and warning which is required
            if (radioBtnCeaser.Checked)
            {
                lblROT.Visible = true;
                txtROT.Visible = true;
                lblKey.Visible = false;
                txtKey.Visible = false;

                lblError.Text = "*Only numbers allowed(<=" + txtAlphabet.Text.Length + ")";

                txtKey.Text = "";
            }
            else
            {
                lblROT.Visible = false;
                txtROT.Visible = false;
                lblKey.Visible = true;
                txtKey.Visible = true;

                lblError.Text = "*Only alphabet characters allowed";

                txtROT.Text = "";
            }
        }

        private void radioBtnOperation_CheckedChanged(object sender, EventArgs e)
        {
            // Call the method which is selected
            if (radioBtnEncrypt.Checked && radioBtnCeaser.Checked)
            {
                // Check correct ROT value
                if (int.Parse(txtROT.Text) > txtAlphabet.Text.Length)
                {
                    lblResult.Text = "Incorrect ROT value!";
                    return;
                }
                lblResult.Text = Encrypt.CeaserChipher(txtString.Text, int.Parse(txtROT.Text), txtAlphabet.Text);
            }
            else if (radioBtnEncrypt.Checked && radioBtnVigenere.Checked)
            {
                lblResult.Text = Encrypt.VigenereChipher(txtString.Text, txtKey.Text, txtAlphabet.Text);
            }
            else if (radioBtnDecrypt.Checked && radioBtnCeaser.Checked)
            {
                // Check correct ROT value
                if (int.Parse(txtROT.Text) > txtAlphabet.Text.Length)
                {
                    lblResult.Text = "Incorrect ROT value!";
                    return;
                }
                lblResult.Text = Decrypt.CeaserCipher(txtString.Text, int.Parse(txtROT.Text), txtAlphabet.Text);
            }
            else if (radioBtnDecrypt.Checked && radioBtnVigenere.Checked)
            {
                lblResult.Text = Decrypt.VigenereCipher(txtString.Text, txtKey.Text, txtAlphabet.Text);
            }
        }

        private void txtAlphabet_TextChanged(object sender, EventArgs e)
        {
            // If alphabet changes, everthing is deleted
            txtString.Text = "";
            txtROT.Text = "";
            txtKey.Text = "";

            lblROT.Visible = false;
            txtROT.Visible = false;
            lblKey.Visible = false;
            txtKey.Visible = false;

            radioBtnCeaser.Checked = false;
            radioBtnVigenere.Checked = false;
            radioBtnEncrypt.Checked = false;
            radioBtnDecrypt.Checked = false;

            lblResult.Text = "";
        }

        private void txtString_TextChanged(object sender, EventArgs e)
        {
            // Enabling groupboxes which is required after string input
            if (txtString.Text.Length != 0)
            {
                // A basic detail for usage
                if (txtKey.Text.Length != 0 || txtROT.Text.Length != 0)
                {
                    grpBoxOps.Enabled = true;
                }
                grpBoxAlgo.Enabled = true;
            }
            else
            {
                grpBoxAlgo.Enabled = false;
                grpBoxOps.Enabled = false;
            }
        }

        private void txtROT_TextChanged(object sender, EventArgs e)
        {
            // Enabling operation groupbox which is required after ROT input
            if (txtROT.Text.Length != 0)
            {
                grpBoxOps.Enabled = true;
            }
            else
            {
                grpBoxOps.Enabled = false;
            }
        }

        private void txtKey_TextChanged(object sender, EventArgs e)
        {
            // Enabling operation groupbox which is required after key value input
            if (txtKey.Text.Length != 0)
            {
                grpBoxOps.Enabled = true;
            }
            else
            {
                grpBoxOps.Enabled = false;
            }
        }

        private void txtROT_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Blocking non-numeric characters on ROT textbox
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtKey_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Blocking characters that is not in the alphabet on key value textbox
            if (!char.IsControl(e.KeyChar) && !txtAlphabet.Text.Contains(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtString_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Blocking characters that is not in the alphabet on string textbox
            if (!char.IsControl(e.KeyChar) && !txtAlphabet.Text.Contains(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void txtAlphabet_KeyPress(object sender, KeyPressEventArgs e)
        {
            // Blocking duplicate characters
            if (!char.IsControl(e.KeyChar) && txtAlphabet.Text.Contains(e.KeyChar))
            {
                e.Handled = true;
            }
        }

        private void windowEncryptDecrypt_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
