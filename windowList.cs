﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_Application
{
    public partial class windowList : Form
    {
        public windowList()
        {
            InitializeComponent();
        }

        private async void btnCalc_Click(object sender, EventArgs e)
        {
            this.Hide();
            await Task.Delay(1000);
            windowCalc calc = new windowCalc();
            calc.Show();
        }

        private async void btnEncDec_Click(object sender, EventArgs e)
        {
            this.Hide();
            await Task.Delay(1000);
            windowEncryptDecrypt encDec = new windowEncryptDecrypt();
            encDec.Show();
        }

        private void windowList_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }

        private async void btnPosG_Click(object sender, EventArgs e)
        {
            this.Hide();
            await Task.Delay(1000);
            PositionGuesser positionGuesser = new PositionGuesser();
            positionGuesser.Show();
        }
    }
}


