﻿
namespace Lab_Application
{
    partial class windowList
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.btnCalc = new System.Windows.Forms.Button();
            this.btnEncDec = new System.Windows.Forms.Button();
            this.btnPosG = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(162)));
            this.label1.Location = new System.Drawing.Point(78, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(103, 42);
            this.label1.TabIndex = 0;
            this.label1.Text = "Application List";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // btnCalc
            // 
            this.btnCalc.Location = new System.Drawing.Point(81, 89);
            this.btnCalc.Name = "btnCalc";
            this.btnCalc.Size = new System.Drawing.Size(100, 23);
            this.btnCalc.TabIndex = 1;
            this.btnCalc.Text = "Calculator";
            this.btnCalc.UseVisualStyleBackColor = true;
            this.btnCalc.Click += new System.EventHandler(this.btnCalc_Click);
            // 
            // btnEncDec
            // 
            this.btnEncDec.Location = new System.Drawing.Point(81, 118);
            this.btnEncDec.Name = "btnEncDec";
            this.btnEncDec.Size = new System.Drawing.Size(100, 23);
            this.btnEncDec.TabIndex = 2;
            this.btnEncDec.Text = "Encrypt-Decrypt";
            this.btnEncDec.UseVisualStyleBackColor = true;
            this.btnEncDec.Click += new System.EventHandler(this.btnEncDec_Click);
            // 
            // btnPosG
            // 
            this.btnPosG.Location = new System.Drawing.Point(81, 147);
            this.btnPosG.Name = "btnPosG";
            this.btnPosG.Size = new System.Drawing.Size(100, 23);
            this.btnPosG.TabIndex = 3;
            this.btnPosG.Text = "Position Guesser";
            this.btnPosG.UseVisualStyleBackColor = true;
            this.btnPosG.Click += new System.EventHandler(this.btnPosG_Click);
            // 
            // windowList
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(261, 214);
            this.Controls.Add(this.btnPosG);
            this.Controls.Add(this.btnEncDec);
            this.Controls.Add(this.btnCalc);
            this.Controls.Add(this.label1);
            this.Name = "windowList";
            this.Text = "windowList";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.windowList_FormClosing);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnCalc;
        private System.Windows.Forms.Button btnEncDec;
        private System.Windows.Forms.Button btnPosG;
    }
}