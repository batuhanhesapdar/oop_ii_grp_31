﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_Application
{
    public partial class windowSignup : Form
    {
        public windowSignup()
        {
            InitializeComponent();
            // To hide passwords
            txtPassword.PasswordChar = '●';
            txtConfirm.PasswordChar = '●';
        }
        
        private void btnSignup_Click(object sender, EventArgs e)
        {
            User userList = User.GetInstance;

            string username = txtUserID.Text;
            string password = Encrypt.MD5Hash(txtPassword.Text);
            string confirm = Encrypt.MD5Hash(txtConfirm.Text);
            // To recognize empty passswords
            string emptyStringHashed = Encrypt.MD5Hash("");

            if (password == confirm && username.Length != 0 && password != emptyStringHashed)
            {
                if(userList.AddUser(username, password))
                {
                    lblError.ForeColor = Color.Green;
                    lblError.Text = "You have successfully signed up. You can go back to login.";
                }
                else
                {
                    lblError.ForeColor = Color.Red;
                    lblError.Text = "This username is already in use.";
                }
            }
            else if (username.Length == 0 || password == emptyStringHashed)
            {
                lblError.ForeColor = Color.Red;
                lblError.Text = "Empty username or password!";
            }
            else
            {
                lblError.ForeColor = Color.Red;
                lblError.Text = "Password and confirmation do not match!";
            }
        }

        private async void btnBack_Click(object sender, EventArgs e)
        {
            await Task.Delay(1000);
            this.Hide();
            windowLogin login = new windowLogin();
            login.Show();
        }

        private void windowSignup_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }
    }   
}
