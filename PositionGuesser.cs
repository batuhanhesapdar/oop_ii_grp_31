﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_Application
{
    public partial class PositionGuesser : Form
    {

        Guesser guesser = new Guesser();
        int x = 512;
        int y = 512;
        public PositionGuesser()
        {
            InitializeComponent();
            lblX.Text = x.ToString();
            lblY.Text = y.ToString();
        }

        private void btnNorth_Click(object sender, EventArgs e)
        {
            y = int.Parse(lblY.Text);
            guesser.guessNorth(ref y);
            lblY.Text = y.ToString();
        }

        private void btnSouth_Click(object sender, EventArgs e)
        {
            y = int.Parse(lblY.Text);
            guesser.guessSouth(ref y);
            lblY.Text = y.ToString();
        }

        private void btnEast_Click(object sender, EventArgs e)
        {
            x = int.Parse(lblX.Text);
            guesser.guessEast(ref x);
            lblX.Text = x.ToString();
        }

        private void btnWest_Click(object sender, EventArgs e)
        {
            x = int.Parse(lblX.Text);
            guesser.guessWest(ref x);
            lblX.Text = x.ToString();
        }

        private void btnSE_Click(object sender, EventArgs e)
        {
            x = int.Parse(lblX.Text);      
            y = int.Parse(lblY.Text);

            guesser.guessSouthEast(ref x, ref y);
          
            lblX.Text = x.ToString();
            lblY.Text = y.ToString();
        }

        private void btnSW_Click(object sender, EventArgs e)
        {
            x = int.Parse(lblX.Text);
            y = int.Parse(lblY.Text);

            guesser.guessSouthWest(ref x, ref y);

            lblX.Text = x.ToString();
            lblY.Text = y.ToString();
        }

        private void btnNW_Click(object sender, EventArgs e)
        {
            x = int.Parse(lblX.Text);
            y = int.Parse(lblY.Text);

            guesser.guessNorthWest(ref x, ref y);

            lblX.Text = x.ToString();
            lblY.Text = y.ToString();
        }

        private void btnNE_Click(object sender, EventArgs e)
        {
            x = int.Parse(lblX.Text);
            y = int.Parse(lblY.Text);

            guesser.guessNorthEast(ref x, ref y);

            lblX.Text = x.ToString();
            lblY.Text = y.ToString();
        }

        private void btnRestart_Click(object sender, EventArgs e)
        {
            guesser = new Guesser();
            lblX.Text = "512";
            lblY.Text = "512";
        }

        private void PositionGuesser_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
