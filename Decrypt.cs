﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Lab_Application
{
    public static class Decrypt
    {
        public static string VigenereCipher(string Text, string KeyValue, string Alphabet)
        {
            List<char> alphabet = stringToCharList(Alphabet);
            string decrypted = "";
            int i = 0;
            foreach (char item in Text)
            {
                int KeyIndex = alphabet.IndexOf(alphabet[alphabet.IndexOf(KeyValue[i % (KeyValue.Length)])]);
                KeyIndex++;

                decrypted += alphabet[(alphabet.IndexOf(item) + alphabet.Count() - KeyIndex) % alphabet.Count()];
                i++;
            }
            return decrypted;
        }

        public static string CeaserCipher(string Text, int RotValue, string Alphabet)
        {
            List<char> alphabet = stringToCharList(Alphabet);
            string decrypted = "";

            foreach (char item in Text)
            {
                decrypted += alphabet[(alphabet.IndexOf(item) + alphabet.Count() - RotValue) % alphabet.Count()];
            }
            return decrypted;
        }

        private static List<char> stringToCharList(string input)
        {
            List<char> list = new List<char>();
            // Eliminate duplicate characters
            for (int i = 0; i < input.Length; i++)
            {
                if (!list.Contains(input[i]))
                {
                    list.Add(input[i]);
                }
            }
            return list;
        }
    }
}
