﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_Application
{
    public class Guesser
    {
        private int upperX = 1024;
        private int lowerX = 0;
        private int upperY = 1024;
        private int lowerY = 0;
        public void guessNorth(ref int y)
        {
            if (y == 1023)
            {
                lowerY = 1024;
            }
            else
            {
                lowerY = y;
            }
            y = (upperY + lowerY) / 2;
        }
        public void guessSouth(ref int y)
        {
            upperY = y;
            y = (upperY + lowerY) / 2;
        }
        public void guessEast(ref int x)
        {
            if (x == 1023)
            {
                lowerX = 1024;
            }
            else
            {
                lowerX = x;
            }
            x = (upperX + lowerX) / 2;
        }
        public void guessWest(ref int x)
        {
            upperX = x;
            x = (upperX + lowerX) / 2;
        }
        public void guessSouthEast(ref int x, ref int y)
        {
            guessEast(ref x);
            guessSouth(ref y);
        }
        public void guessSouthWest(ref int x, ref int y)
        {    
            guessWest(ref x);
            guessSouth(ref y);
        }
        public void guessNorthEast(ref int x, ref int y)
        {   
            guessEast(ref x);
            guessNorth(ref y);
        }
        public void guessNorthWest(ref int x, ref int y)
        {
            guessWest(ref x);
            guessNorth(ref y);
        }

    }
}
