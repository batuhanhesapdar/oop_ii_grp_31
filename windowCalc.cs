﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Lab_Application
{
    public partial class windowCalc : Form
    {
        public windowCalc()
        {
            InitializeComponent();
            // Instantiate the calculate class after initialization
            Calculator calculator = new Calculator(textBox1, textBox2, lblResult);
        }

        // Click and hover events of the add button
        private void btnAdd_Click(object sender, EventArgs e)
        {
            Calculator.OperationHandler("Add");
            ResetAllButtonColors();
            btnAdd.BackColor = Color.SkyBlue;
        }
        private void btnAdd_MouseHover(object sender, EventArgs e)
        {
            btnAdd.ForeColor = Color.Red;
        }
        private void btnAdd_MouseLeave(object sender, EventArgs e)
        {
            btnAdd.ForeColor = Color.Black;
        }

        // Click and hover events of the substract button
        private void btnSub_Click(object sender, EventArgs e)
        {
            Calculator.OperationHandler("Substract");
            ResetAllButtonColors();
            btnSub.BackColor = Color.SkyBlue;
        }
        private void btnSub_MouseHover(object sender, EventArgs e)
        {
            btnSub.ForeColor = Color.Red;
        }
        private void btnSub_MouseLeave(object sender, EventArgs e)
        {
            btnSub.ForeColor = Color.Black;
        }

        // Click and hover events of the multiply button
        private void btnMult_Click(object sender, EventArgs e)
        {
            Calculator.OperationHandler("Multiply");
            ResetAllButtonColors();
            btnMult.BackColor = Color.SkyBlue;
        }
        private void btnMult_MouseHover(object sender, EventArgs e)
        {
            btnMult.ForeColor = Color.Red;
        }
        private void btnMult_MouseLeave(object sender, EventArgs e)
        {
            btnMult.ForeColor = Color.Black;
        }

        // Click and hover events of the divide button
        private void btnDiv_Click(object sender, EventArgs e)
        {
            Calculator.OperationHandler("Divide");
            ResetAllButtonColors();
            btnDiv.BackColor = Color.SkyBlue;
        }
        private void btnDiv_MouseHover(object sender, EventArgs e)
        {
            btnDiv.ForeColor = Color.Red;
        }
        private void btnDiv_MouseLeave(object sender, EventArgs e)
        {
            btnDiv.ForeColor = Color.Black;
        }
        
        // To avoid non-numeric characters in text boxes
        private void textBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            // To allow only numeric character 
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.') && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '-' && (sender as TextBox).Text.Length > 0)
            {
                e.Handled = true;
            }        
        }
        private void textBox2_KeyPress(object sender, KeyPressEventArgs e)
        {
            // To allow only numeric character 
            if (!char.IsControl(e.KeyChar) && !char.IsDigit(e.KeyChar) && (e.KeyChar != '.') && (e.KeyChar != '-'))
            {
                e.Handled = true;
            }

            if ((e.KeyChar == '.') && ((sender as TextBox).Text.IndexOf('.') > -1))
            {
                e.Handled = true;
            }

            if (e.KeyChar == '-' && (sender as TextBox).Text.Length > 0)
            {
                e.Handled = true;
            }
        }

        /// <summary>
        /// Reset all the operation button colors
        /// </summary>
        private void ResetAllButtonColors()
        {
            btnAdd.BackColor = default(Color);
            btnSub.BackColor = default(Color);
            btnMult.BackColor = default(Color);
            btnDiv.BackColor = default(Color);
            btnAdd.UseVisualStyleBackColor = true;
            btnSub.UseVisualStyleBackColor = true;
            btnMult.UseVisualStyleBackColor = true;
            btnDiv.UseVisualStyleBackColor = true;
        }

        private void windowCalc_FormClosing(object sender, FormClosingEventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
