﻿
namespace Lab_Application
{
    partial class windowEncryptDecrypt
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.radioBtnCeaser = new System.Windows.Forms.RadioButton();
            this.radioBtnVigenere = new System.Windows.Forms.RadioButton();
            this.grpBoxOps = new System.Windows.Forms.GroupBox();
            this.radioBtnDecrypt = new System.Windows.Forms.RadioButton();
            this.radioBtnEncrypt = new System.Windows.Forms.RadioButton();
            this.txtAlphabet = new System.Windows.Forms.TextBox();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtString = new System.Windows.Forms.TextBox();
            this.grpBoxAlgo = new System.Windows.Forms.GroupBox();
            this.txtROT = new System.Windows.Forms.TextBox();
            this.txtKey = new System.Windows.Forms.TextBox();
            this.lblROT = new System.Windows.Forms.Label();
            this.lblKey = new System.Windows.Forms.Label();
            this.lblResult = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblError = new System.Windows.Forms.Label();
            this.grpBoxOps.SuspendLayout();
            this.grpBoxAlgo.SuspendLayout();
            this.SuspendLayout();
            // 
            // radioBtnCeaser
            // 
            this.radioBtnCeaser.AutoSize = true;
            this.radioBtnCeaser.Location = new System.Drawing.Point(18, 26);
            this.radioBtnCeaser.Margin = new System.Windows.Forms.Padding(2);
            this.radioBtnCeaser.Name = "radioBtnCeaser";
            this.radioBtnCeaser.Size = new System.Drawing.Size(91, 17);
            this.radioBtnCeaser.TabIndex = 1;
            this.radioBtnCeaser.TabStop = true;
            this.radioBtnCeaser.Text = "Ceaser Cipher";
            this.radioBtnCeaser.UseVisualStyleBackColor = true;
            this.radioBtnCeaser.CheckedChanged += new System.EventHandler(this.radioBtnAlgorithm_CheckedChanged);
            // 
            // radioBtnVigenere
            // 
            this.radioBtnVigenere.AutoSize = true;
            this.radioBtnVigenere.Location = new System.Drawing.Point(18, 47);
            this.radioBtnVigenere.Margin = new System.Windows.Forms.Padding(2);
            this.radioBtnVigenere.Name = "radioBtnVigenere";
            this.radioBtnVigenere.Size = new System.Drawing.Size(99, 17);
            this.radioBtnVigenere.TabIndex = 2;
            this.radioBtnVigenere.TabStop = true;
            this.radioBtnVigenere.Text = "Vigenère cipher";
            this.radioBtnVigenere.UseVisualStyleBackColor = true;
            this.radioBtnVigenere.CheckedChanged += new System.EventHandler(this.radioBtnAlgorithm_CheckedChanged);
            // 
            // grpBoxOps
            // 
            this.grpBoxOps.Controls.Add(this.radioBtnDecrypt);
            this.grpBoxOps.Controls.Add(this.radioBtnEncrypt);
            this.grpBoxOps.Location = new System.Drawing.Point(11, 337);
            this.grpBoxOps.Margin = new System.Windows.Forms.Padding(2);
            this.grpBoxOps.Name = "grpBoxOps";
            this.grpBoxOps.Padding = new System.Windows.Forms.Padding(2);
            this.grpBoxOps.Size = new System.Drawing.Size(131, 80);
            this.grpBoxOps.TabIndex = 3;
            this.grpBoxOps.TabStop = false;
            this.grpBoxOps.Text = "Operations";
            // 
            // radioBtnDecrypt
            // 
            this.radioBtnDecrypt.AutoSize = true;
            this.radioBtnDecrypt.Location = new System.Drawing.Point(18, 47);
            this.radioBtnDecrypt.Name = "radioBtnDecrypt";
            this.radioBtnDecrypt.Size = new System.Drawing.Size(62, 17);
            this.radioBtnDecrypt.TabIndex = 1;
            this.radioBtnDecrypt.Text = "Decrypt";
            this.radioBtnDecrypt.UseVisualStyleBackColor = true;
            this.radioBtnDecrypt.CheckedChanged += new System.EventHandler(this.radioBtnOperation_CheckedChanged);
            // 
            // radioBtnEncrypt
            // 
            this.radioBtnEncrypt.AutoSize = true;
            this.radioBtnEncrypt.Location = new System.Drawing.Point(18, 26);
            this.radioBtnEncrypt.Name = "radioBtnEncrypt";
            this.radioBtnEncrypt.Size = new System.Drawing.Size(61, 17);
            this.radioBtnEncrypt.TabIndex = 0;
            this.radioBtnEncrypt.Text = "Encrypt";
            this.radioBtnEncrypt.UseVisualStyleBackColor = true;
            this.radioBtnEncrypt.CheckedChanged += new System.EventHandler(this.radioBtnOperation_CheckedChanged);
            // 
            // txtAlphabet
            // 
            this.txtAlphabet.Location = new System.Drawing.Point(75, 38);
            this.txtAlphabet.Multiline = true;
            this.txtAlphabet.Name = "txtAlphabet";
            this.txtAlphabet.Size = new System.Drawing.Size(173, 55);
            this.txtAlphabet.TabIndex = 7;
            this.txtAlphabet.TextChanged += new System.EventHandler(this.txtAlphabet_TextChanged);
            this.txtAlphabet.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtAlphabet_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(134, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(52, 13);
            this.label4.TabIndex = 8;
            this.label4.Text = "Alphabet:";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(143, 120);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(37, 13);
            this.label5.TabIndex = 10;
            this.label5.Text = "String:";
            // 
            // txtString
            // 
            this.txtString.Location = new System.Drawing.Point(75, 136);
            this.txtString.Multiline = true;
            this.txtString.Name = "txtString";
            this.txtString.Size = new System.Drawing.Size(173, 67);
            this.txtString.TabIndex = 9;
            this.txtString.TextChanged += new System.EventHandler(this.txtString_TextChanged);
            this.txtString.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtString_KeyPress);
            // 
            // grpBoxAlgo
            // 
            this.grpBoxAlgo.Controls.Add(this.radioBtnCeaser);
            this.grpBoxAlgo.Controls.Add(this.radioBtnVigenere);
            this.grpBoxAlgo.Location = new System.Drawing.Point(11, 252);
            this.grpBoxAlgo.Name = "grpBoxAlgo";
            this.grpBoxAlgo.Size = new System.Drawing.Size(131, 80);
            this.grpBoxAlgo.TabIndex = 11;
            this.grpBoxAlgo.TabStop = false;
            this.grpBoxAlgo.Text = "Algorithms";
            // 
            // txtROT
            // 
            this.txtROT.Location = new System.Drawing.Point(225, 275);
            this.txtROT.Name = "txtROT";
            this.txtROT.Size = new System.Drawing.Size(84, 20);
            this.txtROT.TabIndex = 12;
            this.txtROT.TextChanged += new System.EventHandler(this.txtROT_TextChanged);
            this.txtROT.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtROT_KeyPress);
            // 
            // txtKey
            // 
            this.txtKey.Location = new System.Drawing.Point(225, 296);
            this.txtKey.Name = "txtKey";
            this.txtKey.Size = new System.Drawing.Size(84, 20);
            this.txtKey.TabIndex = 13;
            this.txtKey.TextChanged += new System.EventHandler(this.txtKey_TextChanged);
            this.txtKey.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtKey_KeyPress);
            // 
            // lblROT
            // 
            this.lblROT.AutoSize = true;
            this.lblROT.Location = new System.Drawing.Point(156, 280);
            this.lblROT.Name = "lblROT";
            this.lblROT.Size = new System.Drawing.Size(63, 13);
            this.lblROT.TabIndex = 14;
            this.lblROT.Text = "ROT Value:";
            // 
            // lblKey
            // 
            this.lblKey.AutoSize = true;
            this.lblKey.Location = new System.Drawing.Point(156, 299);
            this.lblKey.Name = "lblKey";
            this.lblKey.Size = new System.Drawing.Size(58, 13);
            this.lblKey.TabIndex = 15;
            this.lblKey.Text = "Key Value:";
            // 
            // lblResult
            // 
            this.lblResult.Location = new System.Drawing.Point(159, 341);
            this.lblResult.Name = "lblResult";
            this.lblResult.Size = new System.Drawing.Size(162, 76);
            this.lblResult.TabIndex = 16;
            this.lblResult.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(220, 328);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(40, 13);
            this.label9.TabIndex = 17;
            this.label9.Text = "Result:";
            // 
            // label1
            // 
            this.label1.ForeColor = System.Drawing.Color.Red;
            this.label1.Location = new System.Drawing.Point(75, 96);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(173, 23);
            this.label1.TabIndex = 18;
            this.label1.Text = "*Duplicate characters not allowed";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label2
            // 
            this.label2.ForeColor = System.Drawing.Color.Red;
            this.label2.Location = new System.Drawing.Point(76, 206);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(173, 23);
            this.label2.TabIndex = 19;
            this.label2.Text = "*Only alphabet characters allowed";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // lblError
            // 
            this.lblError.ForeColor = System.Drawing.Color.Red;
            this.lblError.Location = new System.Drawing.Point(146, 249);
            this.lblError.Name = "lblError";
            this.lblError.Size = new System.Drawing.Size(175, 23);
            this.lblError.TabIndex = 20;
            this.lblError.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // windowEncryptDecrypt
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(333, 447);
            this.Controls.Add(this.lblError);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.lblResult);
            this.Controls.Add(this.lblKey);
            this.Controls.Add(this.lblROT);
            this.Controls.Add(this.txtKey);
            this.Controls.Add(this.txtROT);
            this.Controls.Add(this.grpBoxAlgo);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtString);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtAlphabet);
            this.Controls.Add(this.grpBoxOps);
            this.Margin = new System.Windows.Forms.Padding(2);
            this.Name = "windowEncryptDecrypt";
            this.Text = "th3";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.windowEncryptDecrypt_FormClosing);
            this.grpBoxOps.ResumeLayout(false);
            this.grpBoxOps.PerformLayout();
            this.grpBoxAlgo.ResumeLayout(false);
            this.grpBoxAlgo.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.RadioButton radioBtnCeaser;
        private System.Windows.Forms.RadioButton radioBtnVigenere;
        private System.Windows.Forms.GroupBox grpBoxOps;
        private System.Windows.Forms.TextBox txtAlphabet;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.TextBox txtString;
        private System.Windows.Forms.GroupBox grpBoxAlgo;
        private System.Windows.Forms.TextBox txtROT;
        private System.Windows.Forms.TextBox txtKey;
        private System.Windows.Forms.Label lblROT;
        private System.Windows.Forms.Label lblKey;
        private System.Windows.Forms.Label lblResult;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.RadioButton radioBtnDecrypt;
        private System.Windows.Forms.RadioButton radioBtnEncrypt;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblError;
    }
}